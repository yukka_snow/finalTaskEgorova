from fastapi import FastAPI,HTTPException
from models import *
from redminelib import Redmine
from red_j import migration
from MIGR import get_info, test
from jra_to_red_all import jiraP_to_redP
from red_jira_all import redP_to_JirP
from jira import JIRA
redmine = Redmine('https://task.finch.fm', username='yulia2000.egorova', password='yukka2000')
project = redmine.project.get('finch-train-python')
jira = JIRA(options={'server':'https://fincheyaprobation.atlassian.net'},basic_auth=('yulia2000.egorova@yandex.ru', 'MzQI1LmwGOW6QlB0ffe2DB04'))

app = FastAPI()

@app.post("/red_to_jr")
def migrate_to_jira(is_num: get_issue):
    try:
        migration(is_num.red_id,is_num.jr_id)
    except:
        raise HTTPException(status_code=404, detail="Incorrect data")

@app.post("/jr_to_red")
def migrate_to_redmine(is_num:get_issue):
    try:
        for k in test(is_num.jr_id):
            get_info(k, is_num.red_id)
    except:
        raise HTTPException(status_code=404, detail="Incorrect data")

@app.post('/jraP_to_red')
def jira_to_red(pr_id:get_all):
    try:
        jiraP_to_redP(pr_id.jira_pr,pr_id.red_pr)
    except:
        raise HTTPException(status_code=404, detail="Incorrect data")
@app.post('/redPr_to_jira')
def red_proj_to_jira(pr_id:get_all):
    try:
        redP_to_JirP(pr_id.red_pr,pr_id.jira_pr)
    except:
        raise HTTPException(status_code=404, detail="Incorrect data")

