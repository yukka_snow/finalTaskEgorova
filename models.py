from pydantic import BaseModel

class get_issue(BaseModel):
    red_id: int
    jr_id: str

class get_all(BaseModel):
    red_pr: str
    jira_pr:str