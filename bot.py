import telebot
from telebot import types
from red_j import migration
from red_jira_all import redP_to_JirP
from MIGR import get_info, test
from jra_to_red_all import jiraP_to_redP
bot = telebot.TeleBot('5767764374:AAGv1YJhMbtXQereDrE6A2ZkDHWEH6xO8Es')
@bot.message_handler(commands=['start'])
def start(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("Перенести из редмайна в джиру")
    btn2 = types.KeyboardButton("Перенести из джиры в редмайн")
    markup.add(btn1, btn2)
    bot.send_message(message.chat.id,
                     text="Привет, {0.first_name}! Что ты хочешь сделать?".format(
                         message.from_user), reply_markup=markup)


@bot.message_handler(content_types=['text'])
def func(message):
    if (message.text == "Перенести из редмайна в джиру"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Все задачи")
        btn2 = types.KeyboardButton("Подзадачи одного проекта")
        markup.add(btn1, btn2)
        bot.send_message(message.chat.id, text="Давай поточнее", reply_markup=markup)
    elif (message.text == "Подзадачи одного проекта"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1=types.KeyboardButton("Да")
        back = types.KeyboardButton("Вернуться в главное меню")
        markup.add(btn1, back)
        bot.send_message(message.chat.id, text="Отправь да, если уверен", reply_markup=markup)
    elif message.text=="Да":
        bot.send_message(message.chat.id, text='точно?')
        bot.register_next_step_handler(message, get_id_sub)
    elif message.text=="Вернуться в главное меню":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Перенести из редмайна в джиру")
        btn2 = types.KeyboardButton("Перенести из джиры в редмайн")
        markup.add(btn1, btn2)
        bot.send_message(message.chat.id, text="Вы вернулись в главное меню", reply_markup=markup)

    elif (message.text == "Все задачи"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Продолжим")
        back = types.KeyboardButton("Вернуться в главное меню")
        markup.add(btn1, back)
        bot.send_message(message.chat.id, text="Отправь Продолжим, если уверен", reply_markup=markup)

    elif (message.text == "Продолжим"):
        bot.send_message(message.chat.id, "Прододжим?")
        bot.register_next_step_handler(message, get_id_p)

    elif (message.text == "Перенести из джиры в редмайн"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Все задачи c подзадачами")
        btn2 = types.KeyboardButton("Подзадачи одной задачи")
        markup.add(btn1, btn2)
        bot.send_message(message.chat.id, text="Давай поточнее", reply_markup=markup)
    elif (message.text == "Подзадачи одной задачи"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1=types.KeyboardButton("Ага")
        back = types.KeyboardButton("Вернуться в главное меню")
        markup.add(btn1, back)
        bot.send_message(message.chat.id, text="Отправь Ага, если уверен", reply_markup=markup)
    elif message.text=="Ага":
        bot.send_message(message.chat.id, text='точно?')
        bot.register_next_step_handler(message, get_id_sub_r)
    elif message.text=="Вернуться в главное меню":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Перенести из редмайна в джиру")
        btn2 = types.KeyboardButton("Перенести из джиры в редмайн")
        markup.add(btn1, btn2)
        bot.send_message(message.chat.id, text="Вы вернулись в главное меню", reply_markup=markup)

    elif (message.text == "Все задачи c подзадачами"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Продолжаем")
        back = types.KeyboardButton("Вернуться в главное меню")
        markup.add(btn1, back)
        bot.send_message(message.chat.id, text="Отправь Продолжаем, если уверен", reply_markup=markup)

    elif (message.text == "Продолжаем"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton("Продолжаем")
        back = types.KeyboardButton("Вернуться в главное меню")
        markup.add(btn1, back)
        bot.send_message(message.chat.id, "Уверен?")
        bot.register_next_step_handler(message, get_id_p_r)


    else:
        bot.send_message(message.chat.id, text="На такую комманду я не запрограммировал..")

def get_id_sub(message): #получаем id
    bot.send_message(message.chat.id,text='Введи id задачи в джире и редмайне через запятую')
    bot.register_next_step_handler(message, proc_sub_to_jra)
def proc_sub_to_jra(message):
    bot.send_message(message.chat.id, text='ожидайте, ведется перемещение')
    id_str = message.text
    id_lst = id_str.split(',')
    migration( int(id_lst[-1]),id_lst[0])
    bot.send_message(message.chat.id, text='ready')

def get_id_p(message): #получаем id проектов
    bot.send_message(message.chat.id,text='Введи id проектов в джире и редмайне через запятую')
    bot.register_next_step_handler(message, proc_project_to_jra)

def proc_project_to_jra(message):
    bot.send_message(message.chat.id, text='ожидайте, ведется перемещение')
    id_str = message.text
    id_lst = id_str.split(',')
    redP_to_JirP(id_lst[-1], id_lst[0])
    bot.send_message(message.chat.id, text='ready')

def get_id_p_r(message): #получаем id
    bot.send_message(message.chat.id,text='Введи id проектов в джире и редмайне через запятую')
    bot.register_next_step_handler(message, proc_prod_to_red)
def proc_prod_to_red(message):
    bot.send_message(message.chat.id, text='ожидайте, ведется перемещение задач в редмайн')
    id_str = message.text
    id_lst = id_str.split(',')
    jiraP_to_redP(id_lst[0],id_lst[-1])
    bot.send_message(message.chat.id, text='ready')

def get_id_sub_r(message): #получаем id
    bot.send_message(message.chat.id,text='Введи id задачи в джире и редмайне через запятую')

    bot.register_next_step_handler(message, proc_sub_to_red)
def proc_sub_to_red(message):
    bot.send_message(message.chat.id, text='ожидайте, ведется перемещение в редмайн')
    id_str = message.text
    id_lst = id_str.split(',')
    for k in test(id_lst[0]):
        get_info(k, int(id_lst[-1]))
    bot.send_message(message.chat.id, text='Готово')

bot.polling(none_stop=True)
